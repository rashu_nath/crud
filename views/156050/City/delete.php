<?php
require_once("../../../vendor/autoload.php");

$objCity = new \App\City\City();
$objCity->setData($_GET);
$oneData = $objCity->view();

use App\Message\Message;

if(isset($_GET['Yes']) && $_GET['Yes']==1){
    $objCity->delete();
    $_GET['Yes'] = 0;

}

?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Book Title - Single Book Information</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>

        td{
            border: 0px;
        }

        table{
            border: 1px;
        }

        tr{
            height: 30px;
        }
    </style>



</head>
<body>


<div class="container">
    <h1 style="text-align: center";">Are You Sure You Want To Parmanently Delete The Following Record?</h1>

    <table class="table table-striped table-bordered" cellspacing="0px">


        <tr>
            <th style='width: 10%; text-align: center'>ID</th>
            <th>Book Name</th>
            <th>Author Name</th>
        </tr>

        <?php

        echo "

                  <tr >
                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                     <td>$oneData->name</td>
                     <td>$oneData->city</td>
                  </tr>
              ";

        ?>

    </table>

    <a href='delete.php?id=<?php echo $oneData->id?>&Yes=1' class='btn btn-group-lg btn-info'>Yes</a><!--if the 'yes' button clicked page
                      [(?) is used to passing value by $_get variable]        will be directed to delete.php to do the deleting operation-->
    <a href='index.php' class='btn btn-group-lg btn-info'>No</a><!--if the 'no' button clicked page will be directed to index.php -->





</div>

</body>
</html>