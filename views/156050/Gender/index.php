<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-02-17
 * Time: 08.10
 */

require_once ("../../../vendor/autoload.php");

$objectGender = new \App\Gender\Gender();

//$objectGender->setData($_GET);

$all_data = $objectGender->index();


use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$message = Message::getMessage();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"
    <title>Who are you! I mean he or she....</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">
</head>
<body>
<div class="container">
    <?php echo "<div style='height: 30px; text-align: center'> <div class='alert-success ' id='message'> $message </div> </div>"; ?>
    <div class="nav">

    </div>
    <div class="nav">
        <td> <a class="btn btn-group-lg btn-info" href="trashed.php"> Trashed List </a> </td>
        <td> <a class="btn btn-group-lg btn-info" href="create.php"> Add </a> </td>
    </div>
    <div class="table">
        <h1 style="text-align: center"> ..... Active List ..... </h1>
        <table class="table table-bordered table-striped" cellspacing="0px" border="1px">
            <tr>
                <th style="text-align: center; width:10% "> Serial </th>
                <th style="text-align: center; width:10% "> ID </th>
                <th style="text-align: center"> Name </th>
                <th style="text-align: center "> Gender </th>
                <th style="text-align: center "> Action Butttons </th>
            </tr>
            <?php

            $serial = 1;
            foreach($all_data as $one_data) {

                if ($serial % 2) $background_color = "#cccccc";
                else   $background_color = "#ffffff";

                echo "<tr style='$background_color'>
                                  <td style='text-align: center; width: 10%'> $serial </td>
                                  <td style='text-align: center; width: 10%'> $one_data->id </td>
                                  <td > $one_data->name </td>
                                  <td > $one_data->gender </td>
                                  <td>
                                  <a class='btn btn-group-lg btn-info' href='view.php?id=$one_data->id'> View </a>
                                  <a class='btn btn-group-lg btn-primary' href='edit.php?id=$one_data->id'> Edit </a>
                                  <a class='btn btn-group-lg btn-warning' href='soft_delete.php?id=$one_data->id'> Soft Delete </a>
                                   </td>
                                </tr>";
                $serial++;
            }
            ?>
        </table>
    </div>
</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>


</body>
</html>