
<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='alert-danger' id='message'> $msg </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobby : Add/Create Form</title>



    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">
</head>
<body>
<div class="container">
    <div class="nav">
        <td><a class="btn btn-group-lg btn-primary" href="index.php">Active List</a></td>
    </div>


    <form class="form-group" action="store.php" method="post">

        Name:
        <input class="form-control" type="text" name="name" placeholder="Enter Name Here...">
        <br>

        Gender:
        <input class="checkbox-inline" type="checkbox" name="gender[]" value="Female" > Female
        <input class="checkbox-inline" type="checkbox" name="gender[]" value = "Male"> Male

        <br>

        <input class="form-control" type="submit">

    </form>
</div>


<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>


</body>
</html>