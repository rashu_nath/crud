<?php
require_once ("../../../vendor/autoload.php");

$objectGender = new \App\Gender\Gender();

$trashed_data = $objectGender->trashed();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>trash box</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>


    <style>
        td{
            border:0px;
        }
        tr{
            height: 30px;
        }
    </style>


</head>
<body>
    <div class="container">

        <div class="nav">
            <a class="btn btn-primary" href="index.php">Active List</a>
        </div>

            <h1 class="caption" style="text-align: center;">.....Trashed List.....</h1>

        <table border="1px" class="table table-bordered table-striped" cellspacing="0">
            <tr >
                <th style="text-align: center; width: 10%;"> Serial </th>
                <th style="text-align: center; width: 10%;"> ID </th>
                <th> Name </th>
                <th> Gender</th>
                <th> Action Buttons</th>
            </tr>
            <?php
            $serial = 1;
            foreach($trashed_data as $key) {


                if ($serial%2) $bg_color = "white";
                else $bg_color = "gray";
                echo "<tr style='background-color: $bg_color'>
                            <td style='text-align: center; width: 10%;'> $serial </td>
                            <td style='text-align: center; width: 10%;'> $key->id </td>
                            <td> $key->name </td>
                            <td> $key->gender</td>
                            <td>
                                <a href='recover.php?id=$key->id' class='btn btn-group-lg btn-warning'> Recover </a>
                                <a href='delete.php?id=$key->id' class='btn btn-group-lg btn-danger'> Delete </a>
                            </td>
                    </tr>";
                $serial++;
            }
            ?>

        </table>
    </div>
</body>
</html>