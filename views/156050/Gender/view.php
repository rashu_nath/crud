<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-02-17
 * Time: 03.04
 */


require_once ("../../../vendor/autoload.php");
$objectGender = new \App\Gender\Gender();
$objectGender->setData($_GET);
$one_data = $objectGender->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Single View</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">


</head>
<body>
<div class="container">
    <div class="nav">
        <td> <a class="btn-group-lg btn-info" href="index.php"> Active List </a> </td>
    </div>
    <h1 style="text-align: center"> .....single man's information.....</h1>
    <table class="table-bordered table-striped" border="1px">
        <tr>
            <th style="text-align: center; width:10%"> ID </th>
            <th style="text-align: center"> Name </th>
            <th style="text-align: center"> Gender </th>

        </tr>
        <tr>
            <?php echo "
                        <td style='text-align: center; width: 10%;'> $one_data->id</td>
                        <td style='text-align: center;'> $one_data->name</td>
                        <td style='text-align: center;'> $one_data->gender</td>
                            "; ?>

        </tr>
    </table>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>


</body>
</html>
