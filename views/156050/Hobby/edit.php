<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-02-17
 * Time: 03.47
 */

require_once ("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";


$objectHobby = new \App\Hobby\Hobby();

$objectHobby->setData($_GET);

$one_data = $objectHobby->view();
//var_dump($one_data);
$hobby = explode(',',$one_data->hobby);
//var_dump($hobby);



?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">

    <title> edit </title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">


</head>

<body>

<div class="container">
    <form class="form-control" action="update.php" method="post">
        Name:
        <input class="form-control" type="text" name="name" value="<?php echo $one_data->name ?>">
        <br>
        Hobby:

        <?php

        foreach($hobby as $key){


                echo "<input type='checkbox' class='checkbox-inline' name = hobby[] value = '$key' checked='checked'> $key";
        } ?>

        <input type="hidden" name="id" value="<?php echo $one_data->id ?>">
        <br>

        <input type="submit" class="form-control">



    </form>
</div>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>
</html>
