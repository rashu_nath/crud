<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 05-02-17
 * Time: 00.29
 */

namespace App\ProfilePicture;

use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $picture;
    private $soft_deleted;


    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("name",$postData)){
            $this->name = $postData["name"];
        }

        if(array_key_exists("picture",$postData)){
            $this->picture = $postData['picture'];
        }

        if(array_key_exists("soft_deleted",$postData)){
            $this->soft_deleted = $postData["soft_deleted"];
        }
    }


    public function store(){

        $dataArray = array($this->name,$this->picture) ;
        $sql = "insert into picture(name, picture) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =  $STH->execute($dataArray);
        
        if($result){

            Message::message("Success! :) Data Has Been Inserted!<br>")  ;
        }
        
        else{
            
            Message::message("Failed! :( Data Has Not Been Inserted!<br>")  ;
        }
        
        Utility::redirect('create.php');


    }

    public function index(){

        $sql = "SELECT * FROM picture WHERE soft_deleted='No'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function view(){
        
        $sql = "SELECT * FROM picture WHERE id=".$this->id;
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetch();
    }

    public function update(){

        $dataArray = array($this->name,$this->picture);
        $sql = "UPDATE  picture SET name=?,picture=? where id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result =  $STH->execute($dataArray);

        if($result){

            Message::message("Success! :) Data Has Been Updated!<br>")  ;
        }

        else
        {

            Message::message("Failed! :( Data Has Not Been Updated!<br>")  ;
        }

        Utility::redirect('index.php');
    }

    public function trashed(){

        $sql = "SELECT * FROM picture WHERE soft_deleted='Yes'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function soft_delete(){

        $data_array = array("yes");
        $sql = "UPDATE picture SET soft_deleted=? WHERE id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been soft deleted!<br>");
            Utility::redirect('trashed.php');
        }
        else
        {

            Message::message("Failed! :( Data Has Not Been soft deleted!<br>");
            Utility::redirect('index.php');
        }
    }

    public function recover(){

        $data_array = array("No");
        $sql = "UPDATE picture SET soft_deleted=? WHERE id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been  recovered!<br>");

        }
        else
        {

            Message::message("Failed! :( Data Has Not Been  recovered!<br>");

        }

        Utility::redirect('trashed.php');
    }

    public function delete(){

        $sql = "DELETE FROM picture WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);

        if($result){

            Message::message("Success! :) Data Has Been  deleted!<br>")  ;

        }
        else
        {

            Message::message("Failed! :( Data Has Not Been  deleted!<br>")  ;

        }

        Utility::redirect('index.php');
    }
}