<?php
namespace App\Hobby;

use App\Message\Message;
use App\Utility\Utility;
use PDO;
use App\Model\Database as DB;

class Hobby extends DB
{

    private $id;
    private $name;
    private $hobby;
    private $soft_deleted;


    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("name",$postData)){
            $this->name = $postData["name"];
        }

        if(array_key_exists("hobby",$postData)){
            $this->hobby = implode(',',$postData['hobby']);
        }

        if(array_key_exists("soft_deleted",$postData)){
            $this->soft_deleted = $postData["soft_deleted"];
        }
    }


    public function store(){

        $dataArray = array($this->name,$this->hobby) ;


        $sql = "insert into hobby(name,hobby) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Inserted!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Inserted!<br>")  ;

        }


        Utility::redirect('create.php');


    }

    public function index(){

        $sql = "SELECT * FROM hobby WHERE soft_deleted='No'";

        $sth = $this->DBH->query($sql);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        return $sth->fetchAll();
    }

    public function view(){
        $sql = "SELECT * FROM hobby WHERE id=".$this->id;

        $sth = $this->DBH->query($sql);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        return $sth->fetch();
    }

    public function update(){

        $dataArray = array($this->name,$this->hobby);


        $sql = "UPDATE  hobby SET name=?,hobby=? where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Updated!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Updated!<br>")  ;

        }


        Utility::redirect('index.php');


    }

    public function trashed(){
        $sql = "SELECT * FROM hobby WHERE soft_deleted='Yes'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function soft_delete(){

        $data_array = array("yes");

        $sql = "UPDATE hobby SET soft_deleted=? WHERE id=".$this->id;

        $sth = $this->DBH->prepare($sql);

        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been soft deleted!<br>")  ;
            Utility::redirect('trashed.php');
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been soft deleted!<br>")  ;
            Utility::redirect('index.php');

        }






    }

    public function recover(){
        $data_array = array("No");

        $sql = "UPDATE hobby SET soft_deleted=? WHERE id=".$this->id;

        $sth = $this->DBH->prepare($sql);

        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been  recovered!<br>")  ;

        }
        else
        {
            Message::message("Failed! :( Data Has Not Been  recovered!<br>")  ;


        }

        Utility::redirect('trashed.php');
    }

    public function delete(){
        $sql = "DELETE FROM hobby WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);

        if($result){

            Message::message("Success! :) Data Has Been  deleted!<br>")  ;

        }
        else
        {
            Message::message("Failed! :( Data Has Not Been  deleted!<br>")  ;
            Utility::redirect('trashed.php');

        }
    }






}