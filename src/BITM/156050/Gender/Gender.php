<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 04-02-17
 * Time: 07.36
 */

namespace App\Gender;

use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;
use App\Message\Message;


class Gender extends DB{
    private $id;
    private $name;
    private $gender;
    private $soft_deleted;

    public function setData($post_data){
        if(array_key_exists('id',$post_data)){
            $this->id = $post_data['id'];
        }
        if(array_key_exists('name',$post_data)){
            $this->name = $post_data['name'];
        }
        if(array_key_exists('gender',$post_data)){
            $this->gender = implode(',',$post_data['gender']);
        }
        if(array_key_exists('soft_deleted',$post_data)){
            $this->soft_deleted = $post_data['soft_deleted'];
        }
    }

    public function store(){
        $data_array = array($this->name, $this->gender);
        $sql = "INSERT INTO gender(name,gender) VALUES (?,?)";
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){
            Message::message("data has been stored succesfully!");
        }
        else{
            Message::message("Alas! You are undone....");
        }
        Utility::redirect('create.php');
    }

    public function index(){

        $sql = "SELECT * FROM gender WHERE soft_deleted = 'No'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function view(){

        $sql = "SELECT * FROM gender WHERE id=".$this->id;
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetch();
    }

    public function update(){

        $dataArray = array($this->name,$this->gender);
        $sql = "UPDATE  gender SET name=?,gender=? where id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result =  $STH->execute($dataArray);
        if($result){

            Message::message("Success! :) Data Has Been Updated!<br>");
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Updated!<br>");
        }

        Utility::redirect('index.php');
    }

    public function trashed(){

        $sql = "SELECT * FROM gender WHERE soft_deleted='Yes'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function recover(){

        $data_array = array("No");
        $sql = "UPDATE gender SET soft_deleted=? WHERE id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been  recovered!<br>")  ;

        }
        else
        {
            Message::message("Failed! :( Data Has Not Been  recovered!<br>")  ;
        }

        Utility::redirect('index.php');
    }

    public function soft_delete(){

        $data_array = array("yes");
        $sql = "UPDATE gender SET soft_deleted=? WHERE id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been soft deleted!<br>")  ;
            Utility::redirect('trashed.php');
        }

        else

        {
            Message::message("Failed! :( Data Has Not Been soft deleted!<br>")  ;
            Utility::redirect('index.php');
        }
    }

    public function delete(){

        $sql = "DELETE FROM gender WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);

        if($result){

            Message::message("Success! :) Data Has Been  deleted!<br>")  ;
        }

        else

        {
            Message::message("Failed! :( Data Has Not Been  deleted!<br>")  ;

        }
        Utility::redirect('trashed.php');
    }

}