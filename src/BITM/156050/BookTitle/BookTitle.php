<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Utility\Utility;
use PDO;
use App\Model\Database as DB;

class BookTitle extends DB
{

    private $id;
    private $book_name;
    private $author_name;
    private $soft_deleted;


    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("bookName",$postData)){
            $this->book_name = $postData["bookName"];
        }

        if(array_key_exists("authorName",$postData)){
            $this->author_name = $postData["authorName"];
        }

        if(array_key_exists("soft_deleted",$postData)){
            $this->soft_deleted = $postData["soft_deleted"];
        }
    }


    public function store(){

        $dataArray = array($this->book_name,$this->author_name) ;


        $sql = "insert into book_title(book_name,author_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Inserted!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Inserted!<br>")  ;

        }


        Utility::redirect('index.php');


    }



    public function index(){

        $sql = "select * from book_title where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from book_title where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from book_title where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }




    public function update(){

        $dataArray = array($this->book_name,$this->author_name);


        $sql = "UPDATE  book_title SET book_name=?,author_name=? where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $result =  $STH->execute($dataArray);


        if($result){

            Message::message("Success! :) Data Has Been Updated!<br>")  ;
        }
        else
        {
            Message::message("Failed! :( Data Has Not Been Updated!<br>")  ;

        }


        Utility::redirect('index.php');


    }

    public function soft_delete(){
        $dataArray = array("yes");
        $sql = "UPDATE book_title SET soft_deleted=? where id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success! :) Data Has Been soft deleted!<br>")  ;
        }
        else{
            Message::message("Failed! :( Data Has Not Been soft deleted!<br>")  ;
        }


        Utility::redirect('trashed.php');
    }

    public function delete(){

        $sql = "DELETE FROM book_title WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);

        if($result){
            Message::message("Success! :) Data Has Been  deleted!<br>")  ;
        }
        else{
            Message::message("Failed! :( Data Has Not Been  deleted!<br>")  ;
        }


        Utility::redirect('index.php');
    }

    public function recover(){
        $dataArray = array("No");
        $sql = "UPDATE book_title SET soft_deleted=? where id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($dataArray);

        if($result){
            Message::message("Success! :) Data Has Been recovered!<br>");
        }
        else{
            Message::message("Failed! :( Data Has Not Been recovered!<br>");
        }


        Utility::redirect('index.php');

    }




}












