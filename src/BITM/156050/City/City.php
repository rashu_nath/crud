<?php
/**
 * Created by PhpStorm.
 * User: rashu
 * Date: 05-02-17
 * Time: 14.29
 */

namespace App\City;

use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;


class City extends DB
{
    private $id;
    private $name;
    private $city;
    private $soft_deleted;


    public function setData($postData){

        if(array_key_exists("id",$postData)){
            $this->id = $postData["id"];
        }

        if(array_key_exists("name",$postData)){
            $this->name = $postData["name"];
        }

        if(array_key_exists("city",$postData)){
            $this->city = $postData['city'];
        }

        if(array_key_exists("soft_deleted",$postData)){
            $this->soft_deleted = $postData["soft_deleted"];
        }
    }


    public function store(){

        $dataArray = array($this->name,$this->city) ;
        $sql = "insert into city(name, city) VALUES (?,?)";
        $STH = $this->DBH->prepare($sql);
        $result =  $STH->execute($dataArray);

        if($result){

            Message::message("Success! :) Data Has Been Inserted!<br>")  ;
        }

        else{

            Message::message("Failed! :( Data Has Not Been Inserted!<br>")  ;
        }

        Utility::redirect('create.php');


    }

    public function index(){

        $sql = "SELECT * FROM city WHERE soft_deleted='No'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function view(){

        $sql = "SELECT * FROM city WHERE id=".$this->id;
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetch();
    }

    public function update(){

        $dataArray = array($this->name,$this->city);
        $sql = "UPDATE  city SET name=?,city=? where id=".$this->id;
        $STH = $this->DBH->prepare($sql);
        $result =  $STH->execute($dataArray);

        if($result){

            Message::message("Success! :) Data Has Been Updated!<br>")  ;
        }

        else
        {

            Message::message("Failed! :( Data Has Not Been Updated!<br>")  ;
        }

        Utility::redirect('index.php');
    }

    public function trashed(){

        $sql = "SELECT * FROM city WHERE soft_deleted='Yes'";
        $sth = $this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        return $sth->fetchAll();
    }

    public function soft_delete(){

        $data_array = array("yes");
        $sql = "UPDATE city SET soft_deleted=? WHERE id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been soft deleted!<br>");
            Utility::redirect('trashed.php');
        }
        else
        {

            Message::message("Failed! :( Data Has Not Been soft deleted!<br>");
            Utility::redirect('index.php');
        }
    }

    public function recover(){

        $data_array = array("No");
        $sql = "UPDATE city SET soft_deleted=? WHERE id=".$this->id;
        $sth = $this->DBH->prepare($sql);
        $result = $sth->execute($data_array);

        if($result){

            Message::message("Success! :) Data Has Been  recovered!<br>");

        }
        else
        {

            Message::message("Failed! :( Data Has Not Been  recovered!<br>");

        }

        Utility::redirect('trashed.php');
    }

    public function delete(){

        $sql = "DELETE FROM city WHERE id=".$this->id;
        $result = $this->DBH->exec($sql);

        if($result){

            Message::message("Success! :) Data Has Been  deleted!<br>")  ;

        }
        else
        {

            Message::message("Failed! :( Data Has Not Been  deleted!<br>")  ;

        }

        Utility::redirect('index.php');
    }
}
